#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 15:59:42 2022

@author: alok
"""
from locscale.include.emmer.ndimage.fsc_util import apply_fsc_filter
from locscale.include.emmer.ndimage.map_utils import save_as_mrc

import os
import mrcfile

folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Inputs"
globally_sharpened_map_path = os.path.join(folder, "globally_sharpened_map.mrc")
halfmap_1= os.path.join(folder, "emd_10692_half_map_1.map")
halfmap_2= os.path.join(folder, "emd_10692_half_map_2.map")

globally_sharpened_map = mrcfile.open(globally_sharpened_map_path).data
apix = mrcfile.open(globally_sharpened_map_path).voxel_size.tolist()[0]
filtered_map = apply_fsc_filter(emmap=globally_sharpened_map, apix=apix, halfmap_1=halfmap_1, halfmap_2=halfmap_2)

save_as_mrc(filtered_map[0], output_filename=os.path.join(folder, "globally_sharpened_fsc_filtered.mrc"), apix=apix)

