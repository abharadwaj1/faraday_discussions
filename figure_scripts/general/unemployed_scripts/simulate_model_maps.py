#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 25 15:53:27 2022

@author: alok
"""
# Script to get model maps from PDBs

import os
import mrcfile
import gemmi
from locscale.include.emmer.pdb.pdb_to_map import pdb2map
from locscale.include.emmer.ndimage.map_utils import save_as_mrc

input_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/inputs"

output_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_2/bfactor_correlations_analysis_new_mask"

pdb_prefix = "pdb6y5a_additional_refined_perturbed_strict_masking_no_overlap_rmsd_{}_pm.pdb"

rmsd_magnitudes = [0,100,200,500,1000,1500,2000]
apix = 0.832
size = (320,320,320)

for rmsd in rmsd_magnitudes:
    pdb_path = os.path.join(input_folder, pdb_prefix.format(rmsd))
    output_map_path = os.path.join(output_folder, "pdb6y5a_modelmap_no_overlap_rmsd_{}.mrc".format(rmsd))
    
    simmap = pdb2map(input_pdb=pdb_path, apix=apix,size=size)
    save_as_mrc(simmap, output_map_path, apix=apix)


    
    
    

