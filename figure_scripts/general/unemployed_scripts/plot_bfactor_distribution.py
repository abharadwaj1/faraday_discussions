#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 15:43:41 2022

@author: alok
"""
## Script to plot bfactor distribution of a map

import os
import mrcfile
from locscale.include.emmer.ndimage.map_tools import get_bfactor_distribution, get_atomic_model_mask
import numpy as np

input_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_2/randomise_bfactor/processing_files"

def plot_bfactor_distribution_standard(emmap_path, mask_path, fsc_resolution, showlegend=True):
    from locscale.include.emmer.ndimage.map_tools import get_bfactor_distribution
    import matplotlib.pyplot as plt
    import seaborn as sns
    
    fig, ax =plt.subplots(figsize=(8,8))
    emmap_dist = get_bfactor_distribution(emmap_path, mask_path, fsc_resolution, wilson_cutoff="traditional")
    bfactor_array = np.array([x[0] for x in emmap_dist.values()])

    sns.kdeplot(bfactor_array)
   
    if showlegend:
        plt.legend(["Emmap bfactors"])
    return fig

input_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_2/randomise_bfactor/processing_files"

emmap_path = os.path.join(input_folder, "xyz_emd_10692_additional_1_global_sharpened.mrc")

## get atomic model mask
pdb_path = os.path.join(input_folder, "pdb6y5a_shuffled_bfactor.pdb")

model_mask_path = get_atomic_model_mask(emmap_path, pdb_path)

fsc_resolution = 2.8

plot_bfactor_distribution_standard(emmap_path, mask_path=model_mask_path, fsc_resolution=fsc_resolution)
