#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 20 15:43:41 2022

@author: alok
"""
## Script to plot bfactor distribution of a map

import os
import mrcfile
from locscale.include.emmer.ndimage.map_tools import get_bfactor_distribution, get_atomic_model_mask
import numpy as np
from locscale.include.emmer.ndimage.map_tools import get_bfactor_distribution_multiple
from locscale.utils.plot_utils import plot_correlations_multiple, plot_correlations

input_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_2/bfactor_correlations_analysis_new_mask"


list_of_emmap_path = [os.path.join(input_folder, "pdb6y5a_modelmap_rmsd_{}.mrc".format(rmsd)) for rmsd in [0,100,200,500,1000,1500,2000]]

## get atomic model mask
pdb_path = os.path.join(input_folder, "pdb6y5a_additional_refined_perturbed_strict_masking_rmsd_0_pm.pdb")

model_mask_path = get_atomic_model_mask(list_of_emmap_path[0], pdb_path)

fsc_resolution = 2.8

bfactors_emmap_list = get_bfactor_distribution_multiple(list_of_emmap_path, model_mask_path, fsc_resolution)
bfactor_array_list = {}
for emmap_path in list_of_emmap_path:
    emmap_name = os.path.basename(emmap_path)
    bfactor_array = np.array([x[0] for x in bfactors_emmap_list[emmap_name].values()])
    bfactor_array_list[emmap_name] = bfactor_array




plot_correlations(bfactor_array_list["pdb6y5a_perturbed_0_pm.mrc"], bfactor_array_list["pdb6y5a_perturbed_1000_pm.mrc"],scatter=True)