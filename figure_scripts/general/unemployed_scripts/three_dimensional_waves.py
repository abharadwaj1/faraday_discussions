#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 28 21:17:19 2022

@author: alok
"""
## Script to generate a 3D waveform


import numpy as np
import mrcfile
from locscale.include.emmer.ndimage.map_utils import save_as_mrc
import matplotlib.pyplot as plt

apix=1
size=(200,200,200)

def get_3d_wave(fx = 2,fy = 2,fz = 2):

    [z,y,x] = np.mgrid[-100:100,-100:100,-100:100]
    x = x/x.max()
    y = y/y.max()
    z = z/z.max()
    
    im =  np.sin(2*np.pi*fx * x+2*np.pi*fy * y + 2*np.pi*fz * z)
    return im

wave1 = get_3d_wave(2,0,0)
wave2 = get_3d_wave(0,2,0)
wave3 = get_3d_wave(0,0,2)

save_as_mrc(wave1, "wave_x.mrc",apix=apix)

save_as_mrc(wave2, "wave_y.mrc",apix=apix)

save_as_mrc(wave3, "wave_z.mrc",apix=apix)





