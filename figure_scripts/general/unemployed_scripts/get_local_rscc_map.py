#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 13:44:48 2022

@author: alok
"""
import numpy as np
import os
#import gemmi


def round_up_proper(x):
    epsilon = 1e-5  ## To round up in case of rounding to odd
    return np.round(x+epsilon).astype(int)

def save_list_as_map(values_list, masked_indices, map_shape, map_path, apix):
    from locscale.include.emmer.ndimage.map_utils import save_as_mrc
    value_map = put_scaled_voxels_back_in_original_volume_including_padding(values_list, masked_indices, map_shape)
    save_as_mrc(value_map, output_filename=map_path, apix=apix)
    
def get_central_pixel_vals_after_rscc(emmap_1, emmap_2, masked_xyz_locs, wn, process_name='MPI'):
    from tqdm import tqdm
    from locscale.include.emmer.ndimage.map_tools import compute_real_space_correlation
    from locscale.include.emmer.ndimage.map_utils import extract_window
    from locscale.include.confidenceMapUtil import FDRutil
    import pickle
    
    #if use_theoretical_profile:
    #    print("Using theoretical profiles for Local Scaling with the following parameters: \n")
    #    print(scale_factor_arguments)
    rscc_vals = []
    
    mpi=False
    if process_name == 'MPI':
        mpi=True
        from mpi4py import MPI
        
        comm = MPI.COMM_WORLD
        rank=comm.Get_rank()
        size=comm.Get_size()
        
        pbar = {}
        if rank == 0:
            description = "MPI_RSCC"
            pbar = tqdm(total=len(masked_xyz_locs)*size,desc=description)
        
                
    else:
        progress_bar=tqdm(total=len(masked_xyz_locs), desc=process_name)

    
    for k, j, i in masked_xyz_locs - wn / 2:
        
        try:
        
            k,j,i,wn = round_up_proper(k), round_up_proper(j), round_up_proper(i), round_up_proper(wn)
            
            emmap_1_wn = emmap_1[k:k+wn, j:j+wn, i:i+wn]
            
            emmap_2_wn = emmap_2[k:k+wn, j:j+wn, i:i+wn]
        
            rscc_wn = compute_real_space_correlation(emmap_1_wn, emmap_2_wn)
                
        
            rscc_vals.append(rscc_wn)

            
        except Exception as e:
            print("Rogue voxel detected!  \n")
            print("Location (kji): {},{},{} \n".format(k,j,i))
            print("Skipping this voxel for calculation \n")

            


            print(e)
            print(e.args)
            
            if mpi:
                print("Error occured at process: {}".format(rank))
            
            raise
        
            
        if mpi:
            if rank == 0:
                pbar.update(size)
                
                    
        else:
            progress_bar.update(n=1)
                       
    rscc_vals_array = np.array(rscc_vals, dtype=np.float32)

    return rscc_vals_array

def put_scaled_voxels_back_in_original_volume_including_padding(sharpened_vals, masked_indices, map_shape):
    map_scaled = np.zeros(np.prod(map_shape))
    map_scaled[masked_indices] = sharpened_vals
    map_scaled = map_scaled.reshape(map_shape)

    return map_scaled

def split_sequence_evenly(seq, size):
    """
    >>> split_sequence_evenly(list(range(9)), 4)
    [[0, 1], [2, 3, 4], [5, 6], [7, 8]]
    >>> split_sequence_evenly(list(range(18)), 4)
    [[0, 1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12, 13], [14, 15, 16, 17]]
    """
    newseq = []
    splitsize = 1.0 / size * len(seq)
    for i in range(size):
        newseq.append(seq[round_up_proper(i * splitsize):round_up_proper((i+1) * splitsize)])
    return newseq

def merge_sequence_of_sequences(seq):
    """
    >>> merge_sequence_of_sequences([list(range(9)), list(range(3))])
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2]
    >>> merge_sequence_of_sequences([list(range(9)), [], list(range(3))])
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2]
    """
    newseq = [number for sequence in seq for number in sequence]

    return newseq

def run_local_rscc_calculation(emmap_1, emmap_2, mask, wn):


    from locscale.utils.prepare_inputs import get_xyz_locs_and_indices_after_edge_cropping_and_masking


    
    masked_xyz_locs, masked_indices, map_shape = \
        get_xyz_locs_and_indices_after_edge_cropping_and_masking(mask, wn)


    rscc_vals = get_central_pixel_vals_after_rscc(emmap_1, emmap_2, masked_xyz_locs, wn)

    rscc_map = put_scaled_voxels_back_in_original_volume_including_padding(np.array(rscc_vals),masked_indices, map_shape)


    return rscc_map
  
    
def run_windowed_rscc_calculation(emmap_1, emmap_2, mask, wn):

    from mpi4py import MPI
    from locscale.utils.prepare_inputs import get_xyz_locs_and_indices_after_edge_cropping_and_masking
    
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    if rank == 0:
        masked_xyz_locs, masked_indices, map_shape = \
        get_xyz_locs_and_indices_after_edge_cropping_and_masking(mask, wn)

        zs, ys, xs = masked_xyz_locs.T
        zs = split_sequence_evenly(zs, size)
        ys = split_sequence_evenly(ys, size)
        xs = split_sequence_evenly(xs, size)
    else:
        zs = None
        ys = None
        xs = None

    zs = comm.scatter(zs, root=0)
    ys = comm.scatter(ys, root=0)
    xs = comm.scatter(xs, root=0)

    masked_xyz_locs = np.column_stack((zs, ys, xs))

    process_name = 'MPI'

    rscc_vals = get_central_pixel_vals_after_rscc(emmap_1, emmap_2, masked_xyz_locs, wn, process_name=process_name)
    
    rscc_vals = comm.gather(rscc_vals, root=0)
 

    if rank == 0:
        rscc_vals = merge_sequence_of_sequences(rscc_vals)

        
        rscc_map = put_scaled_voxels_back_in_original_volume_including_padding(np.array(rscc_vals),
        masked_indices, map_shape)
        print("RSCC MAP SHAPE", rscc_map.shape)
        
        
        
                
    else:
        rscc_map = None

    comm.barrier()
    
    if rank==0:
        return rscc_map
    else:
        return None


def get_rscc_map(input_map_1, input_map_2, input_mask, apix=None, window_size_real=25):
    from locscale.include.emmer.ndimage.map_utils import parse_input
    from locscale.include.emmer.ndimage.filter import get_cosine_mask
    import mrcfile
    
    emmap_1 = parse_input(input_map_1)
    emmap_2 = parse_input(input_map_2)
    mask = (get_cosine_mask(parse_input(input_mask), length_cosine_mask_1d=5)>0.5).astype(np.int_)
    
    if apix is None:
        apix=mrcfile.open(input_mask).voxel_size.tolist()[0]
    else:
        apix=apix
    
    window_size_pix = round_up_proper(window_size_real/apix)
    
    rscc_map = run_windowed_rscc_calculation(emmap_1, emmap_2, mask, wn=window_size_pix)
    
    return rscc_map
    
    
import os
import mrcfile
from locscale.include.emmer.ndimage.map_utils import save_as_mrc
     
    
input_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/inputs"
output_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_2/fsc_rscc"
locscale_output_prefix = "locscale_additional_refined_perturbed_strict_masking_no_overlap_rmsd_{}_A.mrc"

mask_path = os.path.join(input_folder, "emd_10692_additional_1_confidenceMap.mrc")
apix=mrcfile.open(mask_path).voxel_size.tolist()[0]

locscale_map_paths = {}
for rmsd in [0,1,2,5,10,15,20]:
    locscale_map_paths[rmsd] = os.path.join(input_folder, locscale_output_prefix.format(rmsd))

local_rscc_maps = {}
for rmsd in [1,2,5,10,15,20]:
    rscc_map = get_rscc_map(input_map_1=locscale_map_paths[0], input_map_2=locscale_map_paths[rmsd], input_mask=mask_path, window_size_real=5)
    if rscc_map is not None:
        local_rscc_maps[rmsd] = rscc_map
        output_filename = os.path.join(output_folder, "local_rscc_map_rmsd_{}_A.mrc".format(rmsd))
        save_as_mrc(map_data=local_rscc_maps[rmsd], output_filename=output_filename, apix=apix)
    else:
        print(":)")



    
