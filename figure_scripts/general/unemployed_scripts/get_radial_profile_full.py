#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 14:49:22 2021

@author: alok
"""

'''
This is a python program to analyse the radial profiles from PDBs in the deposited PDB library
'''
import pickle
from emmer.pdb.pdb_tools import *
from pam_headers import *
from pseudomodel_analysis import *
import json
from scipy import signal

try:
     if len(all_pdb_id) > 0:
          print("Number of PDB ID's identified: "+str(len(all_pdb_id))+'\n')
except:
     all_pdb_id = get_all_pdb_id()
     print("Number of PDB ID's identified: "+str(len(all_pdb_id))+'\n')

     
def standardize_profiles(list_of_profiles,apix,n=10000):
     normalised_profiles = [x/x.max() for x in list_of_profiles]
     common_frequency = np.linspace(1./(float(apix)*10000),1./(float(apix)*2),10000,endpoint=True) 
     interpolated_profiles = []
     for profile in normalised_profiles:
          n = len(profile)
          freq = np.linspace(1./(float(apix)*n),1./(float(apix)*2),n,endpoint=True)
          interpolated_profiles.append(np.interp(common_frequency,freq,profile))
     return common_frequency,interpolated_profiles

def collect_helix_sheet_pdb(pdb_radial_profile):
     '''
     PDB radial profile is a dictionary with pdb id as keys
     '''
     
     helix_only = {}
     sheet_only = {}
     
     for pdb in pdb_radial_profile.keys():
          
          try:
               percent_helix = pdb_radial_profile[pdb]['secondary_structure']['helix']
               percent_sheet = pdb_radial_profile[pdb]['secondary_structure']['sheet']
               print(percent_helix,percent_sheet)
               if percent_helix >= 0.8:
                    helix_only[pdb] = pdb_radial_profile[pdb]
               if percent_sheet >= 0.6:
                    sheet_only[pdb] = pdb_radial_profile[pdb]
          except:
               print("Ignored PDB: "+str(pdb))
               pass
     return [helix_only,sheet_only]

def find_average_secondary_structure_profile(pdb_radial_profile):
     [helix_only,sheet_only] = collect_helix_sheet_pdb(pdb_radial_profile)
     common_freq_helix, interpolated_helix_profiles = standardize_profiles([np.array(x['radial_profile']) for x in helix_only.values()],apix=0.25)
     common_freq_sheet, interpolated_sheet_profiles = standardize_profiles([np.array(x['radial_profile']) for x in sheet_only.values()],apix=0.25)
     

     average_helix_profile = sum(interpolated_helix_profiles) / len(interpolated_helix_profiles)

     average_sheet_profile = sum(interpolated_sheet_profiles) / len(interpolated_sheet_profiles)
     

     average_profiles = {}
     average_profiles['helix'] = [common_freq_helix, average_helix_profile]
     average_profiles['sheet'] = [common_freq_sheet, average_sheet_profile]
          
     return average_profiles
     
pdb_radial_profiles = {}
secondary_structure_distribution = {}
radial_profile = {}
'''
for pdb in random.sample(all_pdb_id,2):
     temp_result = {}
     emmap = pdb_to_map(pdb_id=pdb,dmin=1,vsize=0.25)
     temp_result['secondary_structure'] = get_secondary_structure_residues_content(pdb)
     temp_result['radial_profile'] = tuple(compute_radial_profile(emmap))
     
     
     pdb_radial_profiles[pdb] = temp_result
     
     with open('secon.json', 'w') as f:
         json.dump(pdb_radial_profiles, f)
    
'''
resolution = 1
voxelsize = 0.5

random_sample = random.sample(all_pdb_id,1000)
#if '3J5P' not in random_sample or '3j5p' not in random_sample:
#     random_sample.append('3j5p')
#with open('old_radial_profiles.json','r') as f:
#    old_radial_profiles = json.load(f)
#simulation_list = [x for x in old_radial_profiles.keys()]
    
#print(simulation_list)
with open('pdb_list.pickle','wb') as pdblist:
	pickle.dump(random_sample,pdblist)
print(random_sample)
for pdb in random_sample:
     step = 'initialize'
     try:
          #localfile = pdbl.retrieve_pdb_file(pdb,file_format='pdb')
          pdb_st = get_gemmi_st_from_id(pdb)
          step += ' found_pdb'
          localfile = pdb+"_full.pdb"
          pdb_st.write_pdb(localfile)
          #unit_cell_for_pdb = get_unit_cell_estimate(gemmi.read_pdb(localfile),voxelsize)
        
          set_unit_cell = gemmi.UnitCell(256,256,256,90,90,90)
          helix_model,sheet_model,skipped_residues = split_gemmi_model_based_on_dssp(pdb_st[0],localfile)
          step += ' split_helix'
          helix_st = gemmi.Structure()
          helix_st.add_model(set_model_bfactors(input_model=helix_model,uniform_bfactor=0))
          helix_st.cell = set_unit_cell
          helix_st.write_pdb(pdb+"_helix_zeroB.pdb")
          sheet_st = gemmi.Structure()
          sheet_st.add_model(set_model_bfactors(input_model=sheet_model,uniform_bfactor=0))
          sheet_st.cell = set_unit_cell
          sheet_st.write_pdb(pdb+"_sheet_zeroB.pdb")          
          helix_map = pdb_to_map(pdb_structure=helix_st,vsize=voxelsize,unitcell=set_unit_cell,save_mrc_path=pdb+"_helix.mrc",set_zero_origin=True,crop_to_center=True,perform_fftshift=True,verbose=True)
          sheet_map = pdb_to_map(pdb_structure=sheet_st,vsize=voxelsize,unitcell=set_unit_cell,save_mrc_path=pdb+"_sheet.mrc",set_zero_origin=True,crop_to_center=True,perform_fftshift=True,verbose=True)
          step += ' convert_to_map'
          helix_profile = compute_radial_profile(helix_map).tolist()
          sheet_profile = compute_radial_profile(sheet_map).tolist()
          step += ' compute_radial_profile'
          pdb_result = {}
          pdb_result['helix'] = helix_profile
          pdb_result['sheet'] = sheet_profile
          if len(helix_profile) == len(sheet_profile):
               min_freq = 1/(voxelsize * len(helix_profile))
               max_freq = 1/(voxelsize*2)
               num_samples = len(helix_profile)
               freq = np.linspace(min_freq,max_freq,num_samples,endpoint=True)
               pdb_result['freq'] = freq.tolist()
               pdb_result['apix'] = voxelsize
          else:
               print("Helix and Sheet profiles do not match size. Skipping adding frequency!")
               pdb_result['freq'] = ['skipped']
          step += ' updated_stats'
          pdb_result['stat'] = [round(skipped_residues[1]/skipped_residues[0],2),skipped_residues[0],(max_freq,min_freq),'fraction of residues skipped, total residues, (Nyq freq, min freq)']
          print("Number of skipped residues for PDB-"+pdb+" = "+str(skipped_residues[1])+" out of "+str(skipped_residues[0]))
     
          radial_profile[pdb] = pdb_result
     
          os.remove(localfile)
          os.system('rm -rf */')
     except:
          print("Unexpected error:", sys.exc_info())
          radial_profile[pdb] = [0,step,tuple(sys.exc_info())]
          print(pdb, step)     
#with open('pbd500_M256_L128.json', 'w') as f:
#         json.dump(radial_profile, f)
with open('study_sec_structure_profile_14July.pickle','wb') as f:
	pickle.dump(radial_profile,f)
f.close()
