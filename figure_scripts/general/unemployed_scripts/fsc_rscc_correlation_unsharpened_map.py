#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 16:38:35 2022

@author: alok
"""

## Figure 2 Plots

## Plots required in this figure: FSC and RSCC between Locscale with perturb equal to upto 20A 

import os
import mrcfile
import numpy as np
from locscale.include.emmer.ndimage.fsc_util import calculate_fsc_maps, plot_multiple_fsc
from locscale.include.emmer.ndimage.profile_tools import frequency_array
from locscale.include.emmer.ndimage.map_tools import compute_real_space_correlation, get_atomic_model_mask
from locscale.include.emmer.ndimage.filter import get_cosine_mask
from locscale.include.emmer.pdb.pdb_utils import set_atomic_bfactors
from locscale.include.emmer.pdb.pdb_to_map import pdb2map
import gemmi
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm

input_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/inputs"
output_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_2/fsc_rscc"

pdb_path = os.path.join(input_folder, "pdb6y5a_additional_refined.pdb")
unsharpened_emmap_path = os.path.join(input_folder, "emd_10692_additional_1.map")

locscale_output_prefix = "locscale_additional_refined_perturbed_strict_masking_no_overlap_rmsd_{}_A.mrc"
st = gemmi.read_structure(pdb_path)
model_mask_path = get_atomic_model_mask(unsharpened_emmap_path, pdb_path)
softmask = mrcfile.open(model_mask_path).data
unsharpened_emmap = mrcfile.open(unsharpened_emmap_path).data * softmask
apix = mrcfile.open(unsharpened_emmap_path).voxel_size.tolist()[0]

st_b0 = set_atomic_bfactors(input_gemmi_st=st, b_iso=0)
simmap_bfactor0 = pdb2map(st_b0, apix=apix, size=unsharpened_emmap.shape)
rmsd_magnitudes = [0,1,2,5,10,15,20]
locscale_perturb_map_paths = {}
for rmsd in rmsd_magnitudes:
    locscale_perturb_map_paths[rmsd] = os.path.join(input_folder, locscale_output_prefix.format(rmsd))

apix = mrcfile.open(locscale_perturb_map_paths[0]).voxel_size.tolist()[0]


## Calculation

fsc_arrays_perturb = {}
rscc_perturb = {}
for rmsd in tqdm(rmsd_magnitudes):
    input_map_1 = simmap_bfactor0
    input_map2 = mrcfile.open(locscale_perturb_map_paths[rmsd]).data * softmask
    fsc = calculate_fsc_maps(input_map_1=input_map_1, input_map_2=input_map2)
    rscc = compute_real_space_correlation(input_map_1=input_map_1, input_map_2=locscale_perturb_map_paths[rmsd])
    freq = frequency_array(fsc, apix=apix)
    fsc_arrays_perturb[rmsd] = [freq,fsc]
    rscc_perturb[rmsd] = rscc


#%% Plotting
from matplotlib.pyplot import cm
sns.set_theme(context="paper", font="Helvetica", font_scale=1.5)
sns.set_style("white")
fsc_filename = os.path.join(output_folder, "Figure_2_FSC_locscale_additional_pdb_0bfactor.svg")
colors_rainbow = cm.rainbow(np.linspace(0,1,len(rmsd_magnitudes)))
for i,rmsd in enumerate(rmsd_magnitudes):
    sns.lineplot(x=fsc_arrays_perturb[rmsd][0],y=fsc_arrays_perturb[rmsd][1], linewidth=2, color=colors_rainbow[i])
    plt.xlabel(r" Spatial Frequency, ($\AA^{-1}$)")
    plt.ylabel("FSC")
    
plt.legend(["{} $\AA$".format(x) for x in rmsd_magnitudes])
plt.tight_layout()
plt.savefig(fsc_filename, dpi=600)


#%%%
kwargs = dict(marker="o", linewidth=2, markersize=12)
plt.figure(2)
rscc_filename = os.path.join(output_folder, "Figure_2_RSCC_locscale_additional_pdb_0bfactor.svg")
sns.lineplot(x=rscc_perturb.keys(),y=rscc_perturb.values(), **kwargs)
plt.xlabel("RMSD ($\AA$)")
plt.ylabel("Real Space Correlation")
plt.tight_layout()
plt.savefig(rscc_filename, dpi=600, bbox_inches="tight")


#%%
from scipy.stats import kurtosis
kurtosis_rmsd = {}
for rmsd in rmsd_magnitudes:
    locscale_map = mrcfile.open(locscale_perturb_map_paths[rmsd]).data * softmask
    kurtosis_map = kurtosis(locscale_map.flatten())
    kurtosis_rmsd[rmsd] = kurtosis_map


kwargs = dict(marker="o", linewidth=2, markersize=12)
plt.figure(3)
kurtosis_filename = os.path.join(output_folder, "Figure_2_locscale_map_kurtosis.svg")
sns.lineplot(x=kurtosis_rmsd.keys(),y=kurtosis_rmsd.values(), **kwargs)
plt.xlabel("RMSD ($\AA$)")
plt.ylabel("Map kurtosis")
plt.tight_layout()
plt.savefig(kurtosis_filename, dpi=600, bbox_inches="tight")



