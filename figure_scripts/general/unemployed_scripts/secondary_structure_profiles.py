#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 15:26:29 2021

@author: alok
"""
import scipy.cluster.hierarchy as hac
from scipy.cluster.hierarchy import fcluster
import numpy as np
import matplotlib.pyplot as plt
import pickle
import pandas as pd
from locscale.include.emmer.ndimage.profile_tools import frequency_array, estimate_bfactor_standard, plot_radial_profile, resample_1d
from locscale.include.emmer.pdb.pdb_tools import find_wilson_cutoff
import os

def clean_profile_data(input_dictionary):
    clean_dictionary = {}
    for key_1 in input_dictionary.keys():
        temp_dictionary = {}
        clean = True
       # print(key_1)
        for key_2 in input_dictionary[key_1].keys():
      #      print(key_2)
            arr = input_dictionary[key_1][key_2]
            if not isinstance(arr, np.ndarray):
                continue
           # print(arr)
            else:
                if (not np.all(arr)) and np.isfinite(arr).any() and (not np.isnan(arr).any()):
                    clean = False
                
        if clean:
            
            
            clean_dictionary[key_1] = input_dictionary[key_1]
    
    return clean_dictionary

def normalise(x):
    return x/x.max()

def process_profile(freq, profile):
    '''
    Processing takes place by: taking only wilson region of the profile. Ignore the amplitudes upto 10A

    Parameters
    ----------
    freq : TYPE
        DESCRIPTION.
    profile : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    '''
    if np.isnan(profile).any():
        print("nan")
        return None
    
    if np.any(profile<=0):
        print("zero")
        return None
    
    wilson_cutoff = find_wilson_cutoff(num_atoms=profile[0])

    bfactor = estimate_bfactor_standard(freq, amplitude=profile, wilson_cutoff=wilson_cutoff, fsc_cutoff=1, standard_notation=True)
    
    if bfactor > 10:
        return None
    
    new_freq, new_profile = resample_1d(freq, profile, num=len(profile), xlims=[1/wilson_cutoff,1])
    
    
   # freq_squared = freq**2
   # log_amplitudes = np.log(profile)
    
   # if np.isnan(log_amplitudes).any():
   #     return None
    
    return new_freq, new_profile

def get_code(input_str):
    code = input_str.split("_")[1]
    if code == 'hlx':
        return 'Helix Profile'
    elif code == 'sht':
        return "Sheet Profile"
    elif code == 'rna':
        return "RNA Profile"
    elif code == 'dna':
        return "DNA Profile"
    else:
        return 0
    

input_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_paper/Figures/Figure_3/Input"
output_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_paper/Figures/Figure_3/Output"


radial_profiles_file = os.path.join(input_folder,'secondary_structure_profiles.pickle')
radial_profiles_file_2 = os.path.join(input_folder,'nucleotide_profiles.pickle')

with open(radial_profiles_file,'rb') as f:
    secondary_structure_profile_raw = pickle.load(f)
    
secondary_structure_profile_good = clean_profile_data(secondary_structure_profile_raw)
    
with open(radial_profiles_file_2,'rb') as f:
    nucleotide_profile_raw = pickle.load(f)    
    
nucleotide_profile_good = clean_profile_data(nucleotide_profile_raw)

print("Cleaned input data!")

#%%  Combine data into a single dictionary
radial_profiles = {}

for pdbid in secondary_structure_profile_good.keys():
   radial_profiles[pdbid+"_helix"] = secondary_structure_profile_good[pdbid]['helix']
   radial_profiles[pdbid+"_sheet"] = secondary_structure_profile_good[pdbid]['sheet']
for index in nucleotide_profile_good.keys():
    dna_pdbid = nucleotide_profile_good[index]['dna_id']
    rna_pdbid = nucleotide_profile_good[index]['rna_id']
    radial_profiles[str(rna_pdbid)+"_rna"] = nucleotide_profile_good[index]['rna']
    radial_profiles[str(dna_pdbid)+"_dna"] = nucleotide_profile_good[index]['dna']
## FIND OUTLIERS BASED ON BFACTORS
    
processed_profiles = {}
for pdbid in radial_profiles.keys():
    raw_profile = radial_profiles[pdbid]

    raw_freq = frequency_array(raw_profile,0.5)
    processed = process_profile(raw_freq, raw_profile)
    if processed is not None:
        freq, profile = processed
        processed_profiles[pdbid] = profile
        
    



#%%  Get linkage matrix

#outliers = [x for x in bfactors.keys() if bfactors[x] <= threshold_bfactor]

# CREATE LINKAGE AND PLOT DENDOGRAM

profiles_df = pd.DataFrame(processed_profiles.values(), index=processed_profiles.keys()).T
distance_matrix = profiles_df.corr()
Z = hac.linkage(distance_matrix, method='complete', metric='correlation')

plt.figure(1)
dend=hac.dendrogram(Z, leaf_rotation=90, leaf_font_size=2,labels=list(processed_profiles.keys()), no_labels=True, color_threshold=1.75)
plt.savefig(os.path.join(output_folder, "dendogram.svg"), dpi=600)

#%%  Get cluster

cluster_labels = fcluster(Z, t=2, criterion='maxclust')
print(np.unique(cluster_labels))

profile_cluster = {}
profile_id_cluster = {}

for unique_label in np.unique(cluster_labels):
    profile_cluster[unique_label] = []
    profile_id_cluster[unique_label] = []
    




#%%

profile_id_list = list(processed_profiles.keys())
profile_list = list(processed_profiles.values())

for i,profile_id in enumerate(profile_id_list):
    profile = radial_profiles[profile_id][10:]
    cluster_identity = cluster_labels[i]
    profile_id_cluster[cluster_identity].append(profile_id.split("_")[1])
    profile_cluster[cluster_identity].append(profile)



#%%
figs = {}
for unique_label in np.unique(cluster_labels):
    figs[unique_label] = plot_radial_profile(raw_freq[10:], profile_cluster[unique_label], logScale=False)



#%%

for unique_label in np.unique(cluster_labels):
    plt.figure()
    plt.hist(profile_id_cluster[unique_label])


#%%

def verify_profile(profile):
    if not np.isnan(profile).any() and np.all(profile>0):
        return True
    else:
        return False
    

helix_profiles = []
sheet_profiles = []
rna_profiles = []
dna_profiles = []
crop_first=10
for pdbid in secondary_structure_profile_good.keys():
    helix_pr = secondary_structure_profile_good[pdbid]['helix']
    if verify_profile(helix_pr):
        helix_profiles.append(helix_pr[crop_first:])
    
    sheet_pr = secondary_structure_profile_good[pdbid]['sheet']
    if verify_profile(sheet_pr):
        sheet_profiles.append(sheet_pr[crop_first:])

for index in nucleotide_profile_good.keys():
    rna_pr = nucleotide_profile_good[index]['rna'][crop_first:]
    dna_pr = nucleotide_profile_good[index]['dna'][crop_first:]
    
    if verify_profile(rna_pr):
        rna_profiles.append(rna_pr)
    
    if verify_profile(dna_pr):
        dna_profiles.append(dna_pr)
        
f = frequency_array(profile_size=256, apix=0.5)



#%%

def plot_radial_profile_seaborn(freq, list_of_profiles, font=16, ylims=None):
    import seaborn as sns
    import matplotlib.pyplot as plt
    
    sns.set_theme(context="paper", font="Helvetica", font_scale=1.5)
    sns.set_style("white")
    kwargs = dict(linewidth=3)

    profile_list = np.array(list_of_profiles)
    average_profile = np.einsum("ij->j", profile_list) / len(profile_list)
        
    variation = []
    for col_index in range(profile_list.shape[1]):
        col_extract = profile_list[:,col_index]
        variation.append(col_extract.std())

    variation = np.array(variation)
        
    y_max = average_profile + variation
    y_min = average_profile - variation
    
    fig, ax = plt.subplots()
    ax = sns.lineplot(x=freq, y=average_profile, **kwargs)
    ax.fill_between(freq, y_min, y_max, alpha=0.3)
    ax.set_xlabel('$1/d [\AA^{-1}]$',fontsize=font)
    ax.set_ylabel('$\mid F \mid $',fontsize=font)
    ax2 = ax.twiny()
    ax2.set_xticks(ax.get_xticks())
    ax2.set_xbound(ax.get_xbound())
    ax2.set_xticklabels([round(1/x,1) for x in ax.get_xticks()])
    ax2.set_xlabel('$d [\AA]$',fontsize=font)
    if ylims is not None:
        plt.ylim(ylims)
    plt.tight_layout()
    plt.show()
    
    return fig
    
    
#%%

helix_fig = plot_radial_profile_seaborn(f[crop_first:], helix_profiles, ylims=[-500, 5000])
sheet_fig = plot_radial_profile_seaborn(f[crop_first:], sheet_profiles, ylims=[-500, 5000])
rna_fig = plot_radial_profile_seaborn(f[crop_first:], rna_profiles, ylims=[-500, 5000])
dna_fig = plot_radial_profile_seaborn(f[crop_first:], dna_profiles, ylims=[-500, 5000])

helix_filename = os.path.join(output_folder, "helix_profiles.svg")
sheet_filename = os.path.join(output_folder, "sheet_profiles.svg")
rna_filename = os.path.join(output_folder, "rna_profiles.svg")
dna_filename = os.path.join(output_folder, "dna_profiles.svg")

helix_fig.savefig(helix_filename, dpi=600)
sheet_fig.savefig(sheet_filename, dpi=600)
rna_fig.savefig(rna_filename, dpi=600)
dna_fig.savefig(dna_filename, dpi=600)


#%%
clusters_count = {}
profiles_id_cluster_array_2 = np.array(profile_id_cluster[2])
profiles_id_cluster_array_1 = np.array(profile_id_cluster[1])
for key in ["helix", "sheet", "rna", "dna"]:
    
    count_1 = len(np.where(profiles_id_cluster_array_1==key)[0])
    count_2 = len(np.where(profiles_id_cluster_array_2==key)[0])
    clusters_count[key] = [count_1, count_2]

clusters_df = pd.DataFrame(data=clusters_count, index=["Cluster 1","Cluster 2"]).T
clusters_df.index = ["Helix", "Sheet", "RNA", "DNA"]
axCluster=clusters_df.plot(kind="barh")

for tick in axCluster.get_xticklabels():
     tick.set_fontname("Helvetica")
for tick in axCluster.get_yticklabels():
     tick.set_fontname("Helvetica")

plt.savefig(os.path.join(output_folder, "count_bins.svg"), dpi=600)




#%%

################################# CODE HELL ##################################

'''

#plt.figure(2)
fig = plt.figure(2)
ax1 = fig.add_subplot(111)
ax1.grid(True)
ax2 = ax1.twiny()


get_pdb_for_labels = {}

colors_dictionary = {}

for i,pdbid in enumerate(pdb_labels_arranged):
    
    profile = radial_profiles[pdbid]
    freq = frequency_array(profile,0.5)
    
    if labels_arranged[i] not in get_pdb_for_labels.keys():
        get_pdb_for_labels[labels_arranged[i]] = [pdbid]
    else:
        get_pdb_for_labels[labels_arranged[i]].append(pdbid)
    
    code = get_code(pdbid[-3:])
    if labels_arranged[i] in colors_dictionary.keys():
        colors_dictionary[labels_arranged[i]].append(code)
    else:
        colors_dictionary[labels_arranged[i]] = [code]
        
        
    ax1.plot(freq**2,np.log(profile),c=labels_arranged[i])
    ax1.set_xlabel('r$1/d^2 [\AA^{-2}]$')
    ax1.set_ylabel('log(|F|')
    ax2.set_xlabel('$d [\AA]$')



plt.figure(3,figsize=(16,8))
plt.subplots_adjust(wspace=0.6)
colors = list(set(dend['color_list']))
num_colors = len(colors)
for subplot in range(num_colors):
    subplot_val = 100+num_colors*10+subplot+1
    
    plt.subplot(subplot_val)
    plt.hist(colors_dictionary[colors[subplot]],color=colors[subplot])
    plt.title("Cluster " + str(subplot+1),fontsize=24)
    plt.xticks(rotation=15,fontsize=24)
    



### Find average profiles for Helix, Sheet, RNA and DNA separately

helix_profiles = []
sheet_profiles = []

RNA_profiles = []
DNA_profiles = []
    
for pdbid in selected_profiles.keys():
    identity = pdbid[-3:]
    if identity == 'hlx':
        helix_profiles.append(selected_profiles[pdbid])
    elif identity == 'sht':
        sheet_profiles.append(selected_profiles[pdbid])
    elif identity == 'rna':
        RNA_profiles.append(selected_profiles[pdbid])
    elif identity == 'dna':
        DNA_profiles.append(selected_profiles[pdbid])

helix_profiles = np.array(helix_profiles)
sheet_profiles = np.array(sheet_profiles)
RNA_profiles = np.array(RNA_profiles)
DNA_profiles = np.array(DNA_profiles)

helix_average = np.average(helix_profiles, axis=0)
sheet_average = np.average(sheet_profiles, axis=0)
rna_average = np.average(RNA_profiles, axis=0)
dna_average = np.average(DNA_profiles, axis=0)

freq = frequency_array(profile_1d=helix_average, apix=0.5)


theoretical_profiles = {}

theoretical_profiles['helix'] = {'profile':helix_average, 'freq':freq}
theoretical_profiles['sheet'] = {'profile':sheet_average, 'freq':freq}
theoretical_profiles['rna'] = {'profile':rna_average, 'freq':freq}
theoretical_profiles['dna'] = {'profile':dna_average, 'freq':freq}
'''
