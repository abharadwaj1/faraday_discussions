
import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm
import pickle
from locscale.include.emmer.ndimage.profile_tools import frequency_array, crop_profile_between_frequency, estimate_bfactor_standard
from locscale.include.emmer.pdb.pdb_tools import find_wilson_cutoff
import os
import pwlf

def clean_profile_data(input_dictionary):
    clean_dictionary = {}
    for key_1 in input_dictionary.keys():
        temp_dictionary = {}
        clean = True
       # print(key_1)
        for key_2 in input_dictionary[key_1].keys():
      #      print(key_2)
            arr = input_dictionary[key_1][key_2]
            if not isinstance(arr, np.ndarray):
                continue
           # print(arr)
            else:
                array_is_not_finite = not(np.isfinite(arr).any())
                array_is_nan = np.isnan(arr).any()
                
                
                if array_is_nan or array_is_not_finite:
                    clean = False
                
        if clean:
            
            
            clean_dictionary[key_1] = input_dictionary[key_1]
    
    return clean_dictionary

def normalise(x):
    return x/x.max()

def verify_profile(profile):
    '''
    Processing takes place by: taking only wilson region of the profile. Ignore the amplitudes upto 10A

    Parameters
    ----------
    freq : TYPE
        DESCRIPTION.
    profile : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    '''
    freq, amplitude = profile
    if np.isnan(amplitude).any():
        
        return False
    
    if np.any(amplitude<=0):
        
        return False
    
    wilson_cutoff = find_wilson_cutoff(num_atoms=amplitude[0])

    bfactor = estimate_bfactor_standard(freq, amplitude=amplitude, wilson_cutoff=wilson_cutoff, fsc_cutoff=1, standard_notation=True)
    
    if bfactor > 10:
        return False
    
    else:
        return True
    
def plot_radial_profile_seaborn(freq, list_of_profiles, font=16, ylims=None, crop_first=10, crop_end=1):
    import seaborn as sns
    import matplotlib.pyplot as plt
    
    freq = freq[crop_first:-crop_end]
    
    sns.set_theme(context="paper", font="Helvetica", font_scale=1.5)
    sns.set_style("white")
    kwargs = dict(linewidth=3)

    profile_list = np.array(list_of_profiles)
    average_profile = np.einsum("ij->j", profile_list) / len(profile_list)

    variation = []
    for col_index in range(profile_list.shape[1]):
        col_extract = profile_list[:,col_index]
        variation.append(col_extract.std())

    variation = np.array(variation)
        
    y_max = average_profile + variation
    y_min = average_profile - variation
    
    fig, ax = plt.subplots()
    ax = sns.lineplot(x=freq, y=average_profile[crop_first:-crop_end], **kwargs)
    ax.fill_between(freq, y_min[crop_first:-crop_end], y_max[crop_first:-crop_end], alpha=0.3)
    ax.set_xlabel('Spatial Frequency $1/d [\AA^{-1}]$',fontsize=font)
    ax.set_ylabel('$\mid F \mid $',fontsize=font)
    ax2 = ax.twiny()
    ax2.set_xticks(ax.get_xticks())
    ax2.set_xbound(ax.get_xbound())
    ax2.set_xticklabels([round(1/x,1) for x in ax.get_xticks()])
    ax2.set_xlabel('$d [\AA]$',fontsize=font)
    if ylims is not None:
        plt.ylim(ylims)
    plt.tight_layout()
    plt.show()
    
    return fig


    
#%%
input_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_3/secondary_structures"
output_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_3/secondary_structures"


radial_profiles_file = os.path.join(input_folder,'secondary_structure_profiles.pickle')
radial_profiles_file_2 = os.path.join(input_folder,'nucleotide_profiles.pickle')
selected_pdb_pickle_file = os.path.join(input_folder, "selected_pdb.pickle")

with open(radial_profiles_file,'rb') as f:
    secondary_structure_profile_raw = pickle.load(f)
    
secondary_structure_profile_good = clean_profile_data(secondary_structure_profile_raw)
    
with open(radial_profiles_file_2,'rb') as f:
    nucleotide_profile_raw = pickle.load(f)    

with open(selected_pdb_pickle_file, "rb") as f:
    selected_pdb_id = pickle.load(f)
    
nucleotide_profile_good = clean_profile_data(nucleotide_profile_raw)

print("Cleaned input data!")



helix_profiles = []
sheet_profiles = []
rna_profiles = []
dna_profiles = []

for pdbid in secondary_structure_profile_good.keys():
    if pdbid in selected_pdb_id:
        helix_pr = secondary_structure_profile_good[pdbid]['helix']
        freq = frequency_array(helix_pr, apix=0.5)
        if verify_profile((freq,helix_pr)):
            helix_profiles.append(helix_pr)
    
        sheet_pr = secondary_structure_profile_good[pdbid]['sheet']
        if verify_profile((freq,sheet_pr)):
            sheet_profiles.append(sheet_pr)

for index in nucleotide_profile_good.keys():
    rna_id =  nucleotide_profile_good[index]['rna_id']
    dna_id =  nucleotide_profile_good[index]['dna_id']
    
    if rna_id in selected_pdb_id and dna_id in selected_pdb_id:
        rna_pr = nucleotide_profile_good[index]['rna']
        dna_pr = nucleotide_profile_good[index]['dna']
        
        if verify_profile((freq,rna_pr)):
            rna_profiles.append(rna_pr)
        
        if verify_profile((freq,dna_pr)):
            dna_profiles.append(dna_pr)
            

frequency = frequency_array(profile_size=256, apix=0.5)

column_wise_data_list = []

plot_types = ["Helix Profiles","Sheet Profiles","DNA Profiles","RNA Profiles"]

list_of_list_of_profiles = [helix_profiles, sheet_profiles, dna_profiles]

#%%

def plot_list_of_list_of_radial_profile_seaborn(freq, list_of_list_of_profiles, profile_types, fontscale=3, font="Helvetica",fontsize=28, ylims=None, crop_first=10, crop_end=1):
    import seaborn as sns
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    mpl.rcParams['pdf.fonttype'] = 42
    figsize = (14,8)
    sns.set(rc={'figure.figsize':figsize})
    sns.set_theme(context="paper", font=font, font_scale=fontscale)
    sns.set_style("white")
    freq = freq[crop_first:-crop_end]
    
    sns.set_theme(context="paper", font="Helvetica", font_scale=2)
    sns.set_style("white")
    
    kwargs = dict(linewidth=3)
    fig, ax = plt.subplots(1,len(profile_types))
    for i,list_of_profiles in enumerate(list_of_list_of_profiles):
        num_profiles = len(list_of_profiles)
        profile_text = profile_types[i] + " "+"(N={})".format(num_profiles)
        profile_list = np.array(list_of_profiles)
        average_profile = np.einsum("ij->j", profile_list) / len(profile_list)
    
        variation = []
        for col_index in range(profile_list.shape[1]):
            col_extract = profile_list[:,col_index]
            variation.append(col_extract.std())
    
        variation = np.array(variation)
            
        y_max = average_profile + variation
        y_min = average_profile - variation
        
        
        
        sns.lineplot(x=freq, y=average_profile[crop_first:-crop_end], ax=ax[i], **kwargs)
        
        ax[i].fill_between(freq, y_min[crop_first:-crop_end], y_max[crop_first:-crop_end], alpha=0.3)
        ax[i].text(x=0.5,y=3000,s=profile_text)
        
      
        ax[i].set_xlabel('Spatial Frequency, $d^{-1} [\AA^{-1}]$')
        
        if ylims is not None:
            ax[i].set_ylim(ylims)
        
        if i == 0:
            ax[i].set_ylabel(r'$\langle \mid F \mid \rangle $')
        ax2 = ax[i].twiny()
        ax2.set_xticks(ax[i].get_xticks())
        ax2.set_xbound(ax[i].get_xbound())
        ax2.set_xticklabels([round(1/x,1) for x in ax[i].get_xticks()])
        ax2.set_xlabel('Resolution, $d [\AA]$')

        
    plt.tight_layout()
    plt.show()
    
    return fig

total_fig = plot_list_of_list_of_radial_profile_seaborn(frequency, list_of_list_of_profiles, plot_types, ylims=[-500,4000], fontscale=3)

filename = os.path.join(output_folder, "combined_radial_profiles_with_resolution_version10.eps")
total_fig.savefig(filename, dpi=600, bbox_inches="tight")

#%%
rna_fig = plot_list_of_list_of_radial_profile_seaborn(frequency, [rna_profiles], ["RNA Profiles"], ylims=[-500,4000], fontscale=3)
rna_filename = os.path.join(output_folder, "RNA_radial_profiles_with_resolution_version10.eps")
total_fig.savefig(rna_filename, dpi=600, bbox_inches="tight")