#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  9 17:45:00 2022

@author: alok
"""
#%% Introduction
'''
This script is used to find the effect of scattering atoms on the local bfactor correlation plots

Input: 
    1) refined  atomic model path
    2) number of scattering models (1A, 2A, 5A, 10A etc)
Output: 
    1) local bfactor correlation of atomic model map and scattered atomic model map
    2) local bfactor correlation of scattered atomic model map and atomic bfactors of scattered model
'''

import mrcfile
import gemmi
import os
import numpy as np
import random

from locscale.include.emmer.pdb.pdb_utils import shake_pdb_within_mask
from locscale.include.emmer.pdb.pdb_tools import get_all_atomic_positions, get_atomic_bfactor_window, find_wilson_cutoff
from locscale.include.emmer.ndimage.map_utils import convert_pdb_to_mrc_position
from locscale.include.emmer.ndimage.map_tools import get_local_bfactor_emmap
from locscale.include.emmer.ndimage.profile_tools import compute_radial_profile, frequency_array, estimate_bfactor_standard
from locscale.utils.plot_utils import plot_correlations, plot_correlations_multiple

import seaborn as sns
import matplotlib.pyplot as plt



#%% Inputs
folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_1/Atomic_Guinier_Bfactor"
output_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_1/Atomic_Guinier_Bfactor"


pdb_path = os.path.join(folder, "pdb6y5a_additional_refined.pdb")
mask_path = os.path.join(folder, "emd_10692_additional_1_confidenceMap.mrc")
model_map_path = os.path.join(folder, "pdb6y5a_perturbed_0_pm.mrc")
window_size_A = 25
sample_size = 1000

model_map = mrcfile.open(model_map_path).data
apix = mrcfile.open(model_map_path).voxel_size.tolist()[0]

window_size_pix = int(round(window_size_A/apix))

st = gemmi.read_structure(pdb_path)
atomic_positions = get_all_atomic_positions(gemmi_structure=st)

pdb_centers = random.sample(atomic_positions, sample_size)
mrc_centers = convert_pdb_to_mrc_position(pdb_centers, apix=apix)

atomic_bfactors = []
guinier_bfactors = []
for i,pdb_center in enumerate(pdb_centers):
    mrc_center = mrc_centers[i]
    
    atomic_bfactor = get_atomic_bfactor_window(st, pdb_center, window_size_A)
    guinier_bfactor = get_local_bfactor_emmap(model_map_path, mask_path, mrc_center, fsc_resolution=2*apix)
    
    atomic_bfactors.append(atomic_bfactor)
    guinier_bfactors.append(guinier_bfactor)
    





    
    