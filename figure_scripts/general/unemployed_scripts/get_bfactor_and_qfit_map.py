#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 13:44:48 2022

@author: alok
"""
import numpy as np
import os
#import gemmi


def round_up_proper(x):
    epsilon = 1e-5  ## To round up in case of rounding to odd
    return np.round(x+epsilon).astype(int)

def save_list_as_map(values_list, masked_indices, map_shape, map_path, apix):
    from locscale.include.emmer.ndimage.map_utils import save_as_mrc
    value_map = put_scaled_voxels_back_in_original_volume_including_padding(values_list, masked_indices, map_shape)
    save_as_mrc(value_map, output_filename=map_path, apix=apix)
    
def get_central_bfactor_vals(emmap_1, mask, apix, global_wilson_cutoff,fsc, masked_xyz_locs, wn, cutoff_method, locResData, process_name='MPI'):
    from tqdm import tqdm
    from locscale.include.emmer.ndimage.map_tools import compute_real_space_correlation
    from locscale.include.emmer.pdb.pdb_tools import find_wilson_cutoff
    from locscale.include.emmer.ndimage.profile_tools import estimate_bfactor_standard, compute_radial_profile, frequency_array
    from locscale.include.emmer.ndimage.map_utils import extract_window
    from locscale.include.confidenceMapUtil import FDRutil
    import pickle
    
    #if use_theoretical_profile:
    #    print("Using theoretical profiles for Local Scaling with the following parameters: \n")
    #    print(scale_factor_arguments)
    bfactor_vals = []
    qfit_vals = []
    
    mpi=False
    if process_name == 'MPI':
        mpi=True
        from mpi4py import MPI
        
        comm = MPI.COMM_WORLD
        rank=comm.Get_rank()
        size=comm.Get_size()
        
        pbar = {}
        if rank == 0:
            description = "MPI_Bfactor"
            pbar = tqdm(total=len(masked_xyz_locs)*size,desc=description)
        
                
    else:
        progress_bar=tqdm(total=len(masked_xyz_locs), desc=process_name)

    
    for k, j, i in masked_xyz_locs - wn / 2:
        
        try:
        
            k,j,i,wn = round_up_proper(k), round_up_proper(j), round_up_proper(i), round_up_proper(wn)
            
            emmap_1_wn = emmap_1[k:k+wn, j:j+wn,i:i+wn]
            mask_wn = mask[k:k+wn, j:j+wn,i:i+wn]
            locResData_wn = locResData[k:k+wn, j:j+wn,i:i+wn]
            central_pix = round_up_proper(wn / 2.0)
            
            local_resolution_center = locResData_wn[central_pix,central_pix, central_pix]
            rp_emmap_wn = compute_radial_profile(emmap_1_wn)
            freq = frequency_array(rp_emmap_wn, apix=apix)
            
            if cutoff_method == "singer":
                local_wilson_cutoff = find_wilson_cutoff(num_atoms=rp_emmap_wn[0])
                local_wilson_cutoff = np.clip(local_wilson_cutoff, fsc_resolution*1.5, global_wilson_cutoff)
            else:
                local_wilson_cutoff = 10
            
            if local_resolution_center > 6:
                local_resolution_center = fsc
            
            
            bfactor, qfit = estimate_bfactor_standard(freq, amplitude=rp_emmap_wn, wilson_cutoff=local_wilson_cutoff, fsc_cutoff=local_resolution_center, return_fit_quality=True, standard_notation=True)
            
            bfactor_vals.append(bfactor)
            qfit_vals.append(qfit)
            

            
        except Exception as e:
            print("Rogue voxel detected!  \n")
            print("Location (kji): {},{},{} \n".format(k,j,i))
            print("Skipping this voxel for calculation \n")

            


            print(e)
            print(e.args)
            
            if mpi:
                print("Error occured at process: {}".format(rank))
            
            raise
        
            
        if mpi:
            if rank == 0:
                pbar.update(size)
                
                    
        else:
            progress_bar.update(n=1)
                       
    bfactor_vals_array = np.array(bfactor_vals, dtype=np.float32)
    qfit_vals_array = np.array(qfit_vals, dtype=np.float32)

    return bfactor_vals_array, qfit_vals_array

def put_scaled_voxels_back_in_original_volume_including_padding(sharpened_vals, masked_indices, map_shape):
    map_scaled = np.zeros(np.prod(map_shape))
    map_scaled[masked_indices] = sharpened_vals
    map_scaled = map_scaled.reshape(map_shape)

    return map_scaled

def split_sequence_evenly(seq, size):
    """
    >>> split_sequence_evenly(list(range(9)), 4)
    [[0, 1], [2, 3, 4], [5, 6], [7, 8]]
    >>> split_sequence_evenly(list(range(18)), 4)
    [[0, 1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12, 13], [14, 15, 16, 17]]
    """
    newseq = []
    splitsize = 1.0 / size * len(seq)
    for i in range(size):
        newseq.append(seq[round_up_proper(i * splitsize):round_up_proper((i+1) * splitsize)])
    return newseq

def merge_sequence_of_sequences(seq):
    """
    >>> merge_sequence_of_sequences([list(range(9)), list(range(3))])
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2]
    >>> merge_sequence_of_sequences([list(range(9)), [], list(range(3))])
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 1, 2]
    """
    newseq = [number for sequence in seq for number in sequence]

    return newseq

    
def run_windowed_bfactor_calculation(emmap_1, mask, apix, global_wilson_cutoff, fsc, wn, cutoff_method,locResData):

    from mpi4py import MPI
    from locscale.utils.prepare_inputs import get_xyz_locs_and_indices_after_edge_cropping_and_masking
    
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    if rank == 0:
        masked_xyz_locs, masked_indices, map_shape = \
        get_xyz_locs_and_indices_after_edge_cropping_and_masking(mask, wn)

        zs, ys, xs = masked_xyz_locs.T
        zs = split_sequence_evenly(zs, size)
        ys = split_sequence_evenly(ys, size)
        xs = split_sequence_evenly(xs, size)
    else:
        zs = None
        ys = None
        xs = None

    zs = comm.scatter(zs, root=0)
    ys = comm.scatter(ys, root=0)
    xs = comm.scatter(xs, root=0)

    masked_xyz_locs = np.column_stack((zs, ys, xs))

    process_name = 'MPI'

    bfactor_vals, qfit_vals = get_central_bfactor_vals(emmap_1, mask, apix, global_wilson_cutoff, fsc, masked_xyz_locs, wn, cutoff_method, locResData,process_name)
    
    bfactor_vals = comm.gather(bfactor_vals, root=0)
    qfit_vals = comm.gather(qfit_vals, root=0)
 

    if rank == 0:
        bfactor_vals = merge_sequence_of_sequences(bfactor_vals)
        qfit_vals = merge_sequence_of_sequences(qfit_vals)

        
        bfactor_map = put_scaled_voxels_back_in_original_volume_including_padding(np.array(bfactor_vals),masked_indices, map_shape)
        qfit_map = put_scaled_voxels_back_in_original_volume_including_padding(np.array(qfit_vals),masked_indices, map_shape)
        print("qfit_map MAP SHAPE", qfit_map.shape)
        
        maps = {
            'bfactor': bfactor_map,
            'qfit':qfit_map}
        
        
                
    else:
        maps = {
            'bfactor': None,
            'qfit':None}

    comm.barrier()
    
    if rank==0:
        return maps
    else:
        return None
        

def get_bfactor_qfit_maps(input_map_1, input_mask, fsc_resolution,local_resolution_path, cutoff_method, apix=None, window_size_real=25):
    from locscale.include.emmer.ndimage.map_utils import parse_input
    from locscale.include.emmer.pdb.pdb_tools import find_wilson_cutoff
    import mrcfile
    
    emmap_1 = parse_input(input_map_1)
    mask = parse_input(input_mask)
    locresData = mrcfile.open(local_resolution_path).data
    
    binarised_mask = (mask>=0.5).astype(np.int_)
    if apix is None:
        apix=mrcfile.open(input_mask).voxel_size.tolist()[0]
    else:
        apix=apix
    
    window_size_pix = round_up_proper(window_size_real/apix)
    global_wilson_cutoff = find_wilson_cutoff(mask=binarised_mask, apix=apix)
    maps = run_windowed_bfactor_calculation(emmap_1, binarised_mask, apix=apix, global_wilson_cutoff=global_wilson_cutoff, fsc=fsc_resolution, wn=window_size_pix, cutoff_method=cutoff_method, locResData=locresData)
    
    return maps
    
    
import os
import mrcfile
from locscale.include.emmer.ndimage.map_utils import save_as_mrc
from locscale.include.emmer.ndimage.map_tools import get_atomic_model_mask
from locscale.include.emmer.ndimage.filter import get_cosine_mask
     
    
input_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_1/bfactor_map"
output_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_1/bfactor_map"


cutoff_method = "traditional"
bfactor_filename = "emmap_bfactors_using_{}_cutoff_FDR_mask.mrc".format(cutoff_method)
qfit_filename = "emmap_qfit_using_{}_cutoff_FDR_mask.mrc".format(cutoff_method)

bfactor_full_path = os.path.join(output_folder, bfactor_filename)
qfit_full_path = os.path.join(output_folder, qfit_filename)
                                 
fsc_resolution = 2.8

pdb_path = os.path.join(input_folder, "pdb6y5a_additional_refined.pdb")

emmap_path = os.path.join(input_folder, "emd_10692_additional_1.map")
mask_path = os.path.join(input_folder, "emd_10692_additional_1_confidenceMap.mrc")
local_resolution_path = os.path.join(input_folder, "emd_10692_half_map_1_localResolutions.mrc")

apix=mrcfile.open(emmap_path).voxel_size.tolist()[0]

softmask = get_cosine_mask(mask=mrcfile.open(mask_path).data, length_cosine_mask_1d=7)
softmask_path = os.path.join(input_folder, "emd_10692_confidenceMap_softmask.mrc")
save_as_mrc(softmask, softmask_path,apix=apix)


maps = get_bfactor_qfit_maps(input_map_1=emmap_path, input_mask=softmask_path, 
                             fsc_resolution=fsc_resolution, local_resolution_path=local_resolution_path,cutoff_method=cutoff_method)

if maps is not None:
    save_as_mrc(maps['bfactor'], bfactor_full_path, apix=apix)
    save_as_mrc(maps['qfit'], qfit_full_path, apix=apix)




#%%
'''
import gemmi

poor_qfit_chain = "C"
poor_qfit_res = 585

good_qfit_chain = "B"
good_qfit_res = 311

def crop_box_corner(emmap, corner, wn):
    k,j,i = corner
    crop = emmap[k:k+wn, j:j+wn,i:i+wn]
    
    return crop

def get_mrc_coordinate_from_residue_loc(pdb_path, chain_name, res_id, apix):
    from locscale.include.emmer.ndimage.map_utils import convert_pdb_to_mrc_position
    import gemmi
    
    st = gemmi.read_structure(pdb_path)
    
    chain = st[0][chain_name]
    
    for res in chain:
        if res.seqid.num == res_id:
            ca_atom_pos = res.get_ca().pos.tolist()
    
    pdb_pos = ca_atom_pos
    mrc_pos = convert_pdb_to_mrc_position([pdb_pos], apix)[0]
    
    return mrc_pos

corner_voxel_poor = get_mrc_coordinate_from_residue_loc(pdb_path=pdb_path, chain_name=poor_qfit_chain, res_id=poor_qfit_res, apix=apix)
corner_voxel_good = get_mrc_coordinate_from_residue_loc(pdb_path=pdb_path, chain_name=good_qfit_chain, res_id=good_qfit_res, apix=apix)

emmap = mrcfile.open(emmap_path).data

window_poor = crop_box_corner(emmap, corner_voxel_poor, wn=round_up_proper(25/apix))
window_good = crop_box_corner(emmap, corner_voxel_good, wn=round_up_proper(25/apix))

#%%

from locscale.include.emmer.ndimage.profile_tools import compute_radial_profile, frequency_array, plot_radial_profile, estimate_bfactor_standard
from locscale.include.emmer.pdb.pdb_tools import find_wilson_cutoff

rp_poor = compute_radial_profile(window_poor)
rp_good = compute_radial_profile(window_good)
freq = frequency_array(rp_poor, apix)

## Estimate through traditinal approach
local_wilson_traditional = 10

bfactor_trad_poor, amp_trad_poor, qfit_trad_poor = estimate_bfactor_standard(freq, amplitude=rp_poor, wilson_cutoff=local_wilson_traditional, fsc_cutoff=fsc_resolution, 
                                                    return_fit_quality=True, standard_notation=True, return_amplitude=True)

bfactor_trad_good, amp_trad_good, qfit_trad_good = estimate_bfactor_standard(freq, amplitude=rp_good, wilson_cutoff=local_wilson_traditional, fsc_cutoff=fsc_resolution, 
                                                    return_fit_quality=True, standard_notation=True, return_amplitude=True)


## Estimate through Singer approach
local_wilson_singer_poor = find_wilson_cutoff(num_atoms=rp_poor[0])
local_wilson_singer_good = find_wilson_cutoff(num_atoms=rp_good[0])   
 
bfactor_singer_poor, amp_singer_poor, qfit_singer_poor = estimate_bfactor_standard(freq, amplitude=rp_poor, wilson_cutoff=local_wilson_singer_poor, fsc_cutoff=fsc_resolution, 
                                                    return_fit_quality=True, standard_notation=True, return_amplitude=True)

bfactor_singer_good, amp_singer_good, qfit_singer_good = estimate_bfactor_standard(freq, amplitude=rp_good, wilson_cutoff=local_wilson_singer_good, fsc_cutoff=fsc_resolution, 
                                                    return_fit_quality=True, standard_notation=True, return_amplitude=True)


#%%


exponential_fit_trad_poor = amp_trad_poor * np.exp(-0.25 * bfactor_trad_poor * freq**2)
exponential_fit_trad_good= amp_trad_good * np.exp(-0.25 * bfactor_trad_good * freq**2)
exponential_fit_singer_poor= amp_singer_poor * np.exp(-0.25 * bfactor_singer_poor * freq**2)
exponential_fit_singer_good= amp_singer_good * np.exp(-0.25 * bfactor_singer_good * freq**2)

#%%

fig_poor = plot_radial_profile(freq, [rp_poor, exponential_fit_trad_poor, exponential_fit_singer_poor], 
                    legends=["Reference profile","Exp fit traditional: {}".format(round(bfactor_trad_poor,0)), "Exp fit singer: {}".format(round(bfactor_singer_poor,0))])

fig_good = plot_radial_profile(freq, [rp_good, exponential_fit_trad_good, exponential_fit_singer_good], 
                    legends=["Reference profile","Exp fit traditional: {}".format(round(bfactor_trad_good,0)), "Exp fit singer: {}".format(round(bfactor_singer_good))])

'''