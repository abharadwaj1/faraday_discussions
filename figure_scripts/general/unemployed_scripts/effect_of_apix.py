#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 14:58:42 2022

@author: alok
"""

## Does simulating a map at different apix change the debye effects

import os
import mrcfile
from locscale.include.emmer.ndimage.profile_tools import frequency_array, compute_radial_profile
from locscale.include.emmer.ndimage.profile_tools import plot_radial_profile, get_theoretical_profile, scale_profiles,generate_no_debye_profile
from locscale.include.emmer.ndimage.map_tools import apply_radial_profile, set_radial_profile_to_volume, compute_radial_profile_simple
from locscale.include.emmer.ndimage.map_quality_tools import calculate_surface_area_at_threshold
from locscale.include.emmer.ndimage.map_utils import save_as_mrc
from locscale.include.emmer.pdb.pdb_utils import shake_pdb_within_mask, shake_pdb, set_atomic_bfactors
from locscale.include.emmer.pdb.pdb_to_map import pdb2map
from locscale.utils.plot_utils import pretty_plot_radial_profile
import gemmi
import numpy as np
import seaborn as sns


input_folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/faraday_discussions/Figures/Figure_5"
output_folder = input_folder

pdb_path = os.path.join(input_folder, "pdb_b_424_461.pdb")
st = gemmi.read_structure(pdb_path)

st_0 = set_atomic_bfactors(input_gemmi_st=st,b_iso=300)
apix1 = 0.5
apix2 = 0.832
unitcell = gemmi.UnitCell(266,266,266,90,90,90)

map1 = pdb2map(st_0, unitcell=unitcell,apix=apix1)
map2 = pdb2map(st_0, unitcell=unitcell,apix=apix2)

rp1 = compute_radial_profile(map1)
rp2 = compute_radial_profile(map2)

fig = pretty_plot_radial_profile(frequency_array(rp1,apix1), [rp1],logScale=False, legends=["rp1"],figsize=(20/4,12/4),showlegend=True,
                                  ylims=[-0.005,0.03],fontscale=5/4, linewidth=2, normalise=True, squared_amplitudes=True)
fig = pretty_plot_radial_profile(frequency_array(rp2,apix2), [rp2],logScale=False, legends=["rp2"],figsize=(20/4,12/4),showlegend=True,
                                  ylims=[-0.005,0.03],fontscale=5/4, linewidth=2, normalise=True, squared_amplitudes=True)