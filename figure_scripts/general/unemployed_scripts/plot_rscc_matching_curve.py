#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  7 23:40:24 2022

@author: alok
"""
import seaborn as sns
import matplotlib.pyplot as plt
from locscale.utils.similarity import find_threshold
import os

sns.set_theme(context="paper", font="Helvetica", font_scale=1.5)
sns.set_style("white")
kwargs = dict( linewidth=3)
plt.figure(1)

folder = "/mnt/c/Users/abharadwaj1/Downloads/ForUbuntu/LocScale/faraday_paper/Figures/Figure_2/Plots"
filename= os.path.join(folder, "rscc_matching_threshold.svg")

reference_path = os.path.join(folder,"locscale_perturbed_pdb_0A.mrc")

list_of_target_map_paths=[os.path.join("locscale_perturbed_pdb_{}A.mrc".format(x)) for x in [5,10,15,20]]

t,curve=find_threshold(reference_path, reference_threshold=0.15, 
                       list_of_target_map_paths=list_of_target_map_paths,min_threshold_target=0.02)

for perturb in [5,10,15,20]:
    thresholds = list(curve['locscale_perturbed_pdb_{}A.mrc'.format(perturb)].keys())
    rscc = list(curve['locscale_perturbed_pdb_{}A.mrc'.format(perturb)].values())
    sns.lineplot(x=thresholds, y=rscc, **kwargs)
    plt.xlabel("Threshold")
    plt.ylabel("RSCC")

plt.legend(["RMSD: {} $\AA$".format(x) for x in [5,10,15,20]])
plt.tight_layout()
plt.savefig(filename, dpi=600)